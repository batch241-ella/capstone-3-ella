import './App.css';

import 'font-awesome/css/font-awesome.min.css';

import {useState, useEffect} from 'react';

import {UserProvider} from "./UserContext";

import {BrowserRouter as Router, Route, Routes} from "react-router-dom";

import AppNavbar from "./components/AppNavbar";

import Home from "./pages/Home";
import Products from "./pages/Products";
import ProductView from "./components/ProductView";
import CreateProduct from "./components/CreateProduct";
import Orders from "./pages/Orders";
import Users from "./pages/Users";
import Register from "./pages/Register";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error404 from "./pages/Error404"

import { Container } from "react-bootstrap";


function App() {

  const [cartCount, setCartCount] = useState(localStorage.getItem("cartCount"));

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user information is properly stored upon login and properly cleared upon logout
  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      // User is logged in
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
        // User is logged out
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    }) 
    // Will run only once
}, [])


  return (
    <>
    {/* Inside value are those that we will make global */}
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>

        <AppNavbar cartCount={cartCount} setCartCount={setCartCount}/>
        <Container className="bodyContent">
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/products" element={<Products/>}/>
            <Route path="/products/:productId" element={<ProductView setCartCount={setCartCount}/>}/>
            <Route path="/products/createProduct" element={<CreateProduct/>}/>
            <Route path="/orders" element={<Orders/>}/>
            <Route path="/users" element={<Users/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>

            {/* 404 Route */}
            <Route path="*" element={<Error404/>}/>
          </Routes>
        </Container>
      </Router>
    </UserProvider>
    </>
    );
}

export default App;
