import { Card, Button, Container, Image } from "react-bootstrap";

import {Link} from 'react-router-dom';

export default function ProductCard({product}) {

	const {name, description, price, _id} = product;


	return (
	<Card className="my-2">
	    <Card.Body>
	    	<Container className="p-0 mb-3 d-flex align-items-center justify-content-center">
	    		<Image fluid src="https://images.pexels.com/photos/20787/pexels-photo.jpg?auto=compress&cs=tinysrgb&h=350" />
	    	</Container>
	        <Card.Title className="text-olive">{name}</Card.Title>
	        {/*<Card.Subtitle>Description:</Card.Subtitle>*/}
	        <Card.Text>{description}</Card.Text>
	        {/*<Card.Subtitle>Price:</Card.Subtitle>*/}
	        <Card.Text className="text-orange">PhP {price}</Card.Text>
			<Button variant="warning" className="detailsBtn" as={Link} to={`/products/${_id}`}>Details</Button> 
	    </Card.Body>
	</Card>
	)
}