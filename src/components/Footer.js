import {Stack} from 'react-bootstrap';

export default function Footer() {
    return (
   	<footer className="d-flex justify-content-center align-content-center">
   		<Stack direction="vertical" gap={1}>
   		<div className="text-center text-cream mt-3"> 
   			<h4>Contact Us</h4>
   		</div>
		<div className="text-center bg-olive pb-3">
			<a href="https://en.wikipedia.org/wiki/Pizza" target="_blank" rel="noreferrer"><i class="fa fa-facebook-square" id="facebook"></i></a>
			<a href="https://en.wikipedia.org/wiki/Pizza" target="_blank" rel="noreferrer"><i class="fa fa-twitter" id="linkedin"></i></a>
			<a href="https://en.wikipedia.org/wiki/Pizza" target="_blank" rel="noreferrer"><i class="fa fa-envelope" id="jobstreet"></i></a>
			<a href="https://en.wikipedia.org/wiki/Pizza" target="_blank" rel="noreferrer"><i class="fa fa-phone" id="github"></i></a>
		</div>
		</Stack>
	</footer>
    )
}