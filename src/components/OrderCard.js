import {useState, useEffect, Fragment} from 'react';

import { Card, Accordion, Stack } from "react-bootstrap";

import Moment from 'moment';

import "../App.css";


export default function OrderCard({order, count, isAdmin}) {

	const [productItems, setProducts] = useState([]);

	const {totalAmount, purchasedOn} = order;

	// useEffect(() => {
	// 	setProducts(products.map(product => {
	// 		return (
	// 			<Fragment key={product._id}>
	// 			<Card.Subtitle>Product ID:</Card.Subtitle>
	// 			<Card.Text>{product.productId}</Card.Text>
	// 			<Card.Subtitle>Quantity:</Card.Subtitle>
	// 			<Card.Text>{product.quantity}</Card.Text>
	// 	        </Fragment>
	// 	    )
	// 	}))
	// }, [])
	
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/orders/${order._id}/getSubtotal`, {
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
		})
		.then(res => res.json())
		.then(productSubtotalArr => {
			let count = 0;
			setProducts(productSubtotalArr.map(productSubtotal => {
				count++;
				return (
					<Fragment key={productSubtotal._id}>
					<Card className="mb-3" style={{backgroundColor: "#d3d3d3"}}>
						<Card.Body>
							<Card.Subtitle style={{fontWeight: "bold"}}>Product #{count}</Card.Subtitle>
							<Card.Text><b>Name: </b>{productSubtotal.name}</Card.Text>
							<Card.Text><b>Price: </b>{productSubtotal.price}</Card.Text>
							<Card.Text><b>Quantity: </b>{productSubtotal.quantity}</Card.Text>
							<Card.Text><b>Subtotal: </b>{productSubtotal.subtotal}</Card.Text>
						</Card.Body>
					</Card>
			        </Fragment>
			    )
			}))
		})

	}, [order._id])


	return (
	// <Card>
	//     <Card.Body>
	//         <Card.Title>Order ID: {order._id}</Card.Title>
	//         {productItems}
	//         <Card.Subtitle>Total Amount:</Card.Subtitle>
	//         <Card.Text>{totalAmount}</Card.Text>
	//     </Card.Body>
	// </Card>
		<Accordion>
	      <Accordion.Item eventKey="0">
	        <Accordion.Header><p><b>{ isAdmin ? `User ${count}` : `Order #${count}`}</b> purchased on {Moment(purchasedOn).format('DD/MM/YYYY hh:mm A')}</p></Accordion.Header>
	        <Accordion.Body>
	          {productItems}
	          <Stack direction="horizontal" gap={1}className="mx-1">
	          	<div className="font-weight-bold">Purchase Date:</div>
	          	<div>{Moment(purchasedOn).format('dddd, MMMM D, YYYY hh:mm A')}</div>
	          </Stack>
	          <Stack direction="horizontal" gap={1} className="mx-1">
	          	<div className="font-weight-bold">Total Amount: </div>
	          	<div className="text-warning">{totalAmount}</div>
	          </Stack>
	        </Accordion.Body>
	      </Accordion.Item>
	    </Accordion>

	)
}