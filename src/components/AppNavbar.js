import {useState, useEffect, useContext, Fragment, useMemo, useCallback} from 'react';

import {Link, NavLink, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import {Card, Offcanvas, Button, Row, Col} from 'react-bootstrap';

import Swal from 'sweetalert2';

export default function AppNavbar({cartCount, setCartCount}) {

  const navigate = useNavigate();

  const { user } = useContext(UserContext);
  const isAdmin = localStorage.getItem("isAdmin") === "true";

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleRedirect = useCallback(() => {
    navigate("/products");
    handleClose();
  }, [navigate]);

  const handleCardClick = () => {
    Swal.fire(
      'Update Product Quantity?',
      'Delete product be setting quantity to zero',
      'question'
    )
  }

  let orderId = localStorage.getItem("orderId");

  const emptyCartDisplay = useMemo(() => (
    <Row>
        <Col className="p-5">
            <h5>Cart is Empty</h5>
            <p>Let's add some products to our cart!</p>
            <Button variant="danger" className="orderBtn" onClick={handleRedirect}>Add to Cart</Button>
        </Col>
    </Row>
  ), [handleRedirect]);

  const [cartBody, setCartBody] = useState(emptyCartDisplay);

 
  useEffect(() => {

     function checkoutOrder() {
      Swal.fire({
          title: "Order Confirmed",
          icon: "success",
          text: "Thank you for your purchase!"
      })
      // set order as archived

      // unset current order
      localStorage.removeItem("orderId");
      // set cart back to empty
      localStorage.setItem("isCartEmpty", true);
      // set cart count to zero
      localStorage.setItem("cartCount", 0);
      setCartCount(0);

      // redirect to home
      navigate("/");
      // close offcanvas
      handleClose();
    }


    if (typeof orderId !== "undefined" && orderId) {
      console.log("triggerred")
      fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/getOrder`, {
          method: 'GET',
          headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`
          }
      })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        // get subtotal for each product
        fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/getSubtotal`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(productSubtotalArr => {
          let count = 0;
          let productCards = productSubtotalArr.map(productSubtotal => {
            count++;
            return (
                <Fragment key={productSubtotal._id}>
                <Card className="mb-3 bg-gray cartCard" onClick={e => handleCardClick()}>
                  <Card.Body>
                    <Card.Subtitle style={{fontWeight: "bold"}}>Product #{count}</Card.Subtitle>
                    <Card.Text><b>Name: </b>{productSubtotal.name}</Card.Text>
                    <Card.Text><b>Price: </b>{productSubtotal.price}</Card.Text>
                    <Card.Text><b>Quantity: </b>{productSubtotal.quantity}</Card.Text>
                    <Card.Text><b>Subtotal: </b>{productSubtotal.subtotal}</Card.Text>
                  </Card.Body>
                </Card>
                </Fragment>
              )
          })

          setCartBody(
            <div>
              {productCards}
              <h6>Total Amount:</h6>
              <p>{data.totalAmount}</p>
              <div className="mt-1 d-flex align-items-center justify-content-center">
                <Button variant="success" className="orderBtn" onClick={checkoutOrder}>Checkout Order</Button>
              </div>
            </div>
          );

        });

      });

    } else {
      setCartBody(emptyCartDisplay);
    }

  }, [orderId, cartCount, setCartCount, emptyCartDisplay, navigate])

  return (
    <>
    <Navbar bg="light" expand="lg">
        <Navbar.Brand className="mx-3" as={Link} to="/">
          <img src={require('../images/pizza-logo.svg').default} alt="..." height="30" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mx-3 m-lg-auto">
            <Nav.Link className="navText" as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link className="navText" as={NavLink} to="/products">{isAdmin ? "Products" : "Menu"}</Nav.Link>
            { (user.id !== null) ?
              <>
              { isAdmin ?
                <>
                <Nav.Link className="navText" as={NavLink} to="/orders">Orders</Nav.Link>
                <Nav.Link className="navText" as={NavLink} to="/users">Users</Nav.Link>
                </>
                :
                <>
                <Nav.Link className="navText" as={NavLink} to="/orders">Order History</Nav.Link>
                <Nav.Link onClick={handleShow}><span className="navText"><i className="fa fa-lg fa-shopping-cart"></i> Order Online</span></Nav.Link>
                </>
              }
              <Nav.Link className="navText" as={NavLink} to="/logout">Logout</Nav.Link>
              </>
              :
              <>
              <Nav.Link className="navText register" as={NavLink} to="/register">Register</Nav.Link>
              <Nav.Link className="navText" as={NavLink} to="/login">Login</Nav.Link>
              </>
            }
          </Nav>
        </Navbar.Collapse>
    </Navbar>

    <Offcanvas show={show} onHide={handleClose} placement="end">
      <Offcanvas.Header closeButton>
        <Offcanvas.Title>My Cart</Offcanvas.Title>
      </Offcanvas.Header>
      <Offcanvas.Body>
        <p>You currently have <b>{cartCount || 0}</b> items in your cart</p>
        {cartBody}
      </Offcanvas.Body>
    </Offcanvas>
    </>
  );
}


