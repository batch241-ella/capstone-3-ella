import { Button, Row, Col } from 'react-bootstrap';

import { Link } from 'react-router-dom';

export default function Banner({data}) {

    const {title, content, destination, label} = data;

    return (
    <Row>
        <Col className="p-5">
            <h1 className="heading-lg">{title}</h1>
            <p>{content}</p>
            <Button className="bannerBtn" variant="dark" as={Link} to={destination}>{label}</Button>
        </Col>
    </Row>
    )
}