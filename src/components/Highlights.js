import { Row, Col, Card, Container, Image } from "react-bootstrap";

export default function Highlights() {
	return (
	    <Row className="mt-3 mb-3">
	        <Col xs={12} md={4} className="my-2">
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                	<Container className="mb-3">
	                		<Image fluid src={require('../images/delivery.svg').default} />
	                	</Container>
	                    <Card.Title>
	                        <h2>Speedy Delivery</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        Freshly made pizza to your doorstep in less than 30 minutes 
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4} className="my-2">
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                	<Container className="mb-3">
	                		<Image fluid src={require('../images/seal.svg').default} />
	                	</Container>
	                    <Card.Title>
	                        <h2>Seal of Approval</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        You can always expect quality from our products every time you order. Don't like the food? We provide a full refund if it's not up to your taste.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4} className="my-2">
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                	<Container className="mb-3">
	                		<Image fluid src={require('../images/discount.svg').default} />
	                	</Container>
	                    <Card.Title>
	                        <h2>Exclusive Deals</h2>
	                    </Card.Title>
	                    <Card.Text>
	                        Weekly promos and discounts can be found on our website and our socials. 
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	    </Row>
	)
}