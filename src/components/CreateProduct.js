import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

import {useNavigate, Link} from 'react-router-dom';

import Swal from 'sweetalert2';

export default function CreateProduct() {

	const navigate = useNavigate();

	const [productName, setProductName] = useState('');
	const [productDescription, setProductDescription] = useState('');
	const [productPrice, setProductPrice] = useState('');

	const [isActive, setIsActive] = useState(false);

	const Toast = Swal.mixin({
	  toast: true,
	  position: 'top-end',
	  showConfirmButton: false,
	  timer: 3000,
	  timerProgressBar: true,
	  didOpen: (toast) => {
	    toast.addEventListener('mouseenter', Swal.stopTimer)
	    toast.addEventListener('mouseleave', Swal.resumeTimer)
	  }
	})

	function addProduct(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: productName,
				description: productDescription,
				price: productPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data) {
				Toast.fire({
				  icon: 'success',
				  title: 'Added product to database'
				})
				navigate("/products");
			} else {
			    Toast.fire({
				  icon: 'danger',
				  title: 'Something went wrong'
				})
			}
		})

		setProductName('');
		setProductDescription('');
		setProductPrice('');
	}


	useEffect(() => {
		if (productName && productDescription && productPrice) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [productName, productDescription, productPrice])


    return (
        <Form onSubmit={e => addProduct(e)} className="mt-3">
            <Form.Group controlId="productName" className="my-3">
                <Form.Label>Name</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter product name" 
	                value={productName}
	                onChange={e => setProductName(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="productDescription" className="my-3">
                <Form.Label>Description</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter product description" 
	                value={productDescription}
	                onChange={e => setProductDescription(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="productPrice" className="my-3">
                <Form.Label>Price</Form.Label>
                <Form.Control 
	                type="number" 
	                placeholder="Enter product price"
	                min="0" 
	                value={productPrice}
	                onChange={e => setProductPrice(e.target.value)}
	                required
                />
            </Form.Group>

            { isActive ?
	            <Button variant="success" type="submit" id="submitBtn">
	            	Submit
	            </Button>
            :
	            <Button variant="success" type="submit" id="submitBtn" disabled>
	            	Submit
	            </Button>
            }
            <Button className="bg-secondary mx-2" as={Link} to={`/products`}>Cancel</Button>
        </Form>
    )

}