import { Form, Card, Button, Badge } from "react-bootstrap";

import { useState } from 'react';

import Swal from "sweetalert2";

export default function AdminProductView({product}) {

	const {name, description, price, isActive, _id} = product;

	const [productIsActive, setProductIsActive] = useState(isActive);

	const [showUpdateForm, setShowUpdateForm] = useState(false);

	const [productName, setProductName] = useState(name);
	const [productDescription, setProductDescription] = useState(description);
	const [productPrice, setProductPrice] = useState(price);

	const [displayProductName, setDisplayProductName] = useState(name);
	const [displayProductDescription, setDisplayProductDescription] = useState(description);
	const [displayProductPrice, setDisplayProductPrice] = useState(price);


	const showForm = () => setShowUpdateForm(!showUpdateForm);

	const Toast = Swal.mixin({
	  toast: true,
	  position: 'top-end',
	  showConfirmButton: false,
	  timer: 1500,
	  timerProgressBar: true,
	  didOpen: (toast) => {
	    toast.addEventListener('mouseenter', Swal.stopTimer)
	    toast.addEventListener('mouseleave', Swal.resumeTimer)
	  }
	});


	function archiveProduct(e, productId, isActive) {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data) {
				setProductIsActive(isActive);
				let message = isActive ? "Reactivated" : "Deactivated";
				Toast.fire({
				  icon: 'success',
				  title: `${message} Product`
				})
			} else {
				Toast.fire({
				  icon: 'error',
				  title: 'Something went wrong'
				})
			}
		});
	}

	function updateProduct(e, productId) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: productName,
				description: productDescription,
				price: productPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data) {
				setDisplayProductName(productName);
				setDisplayProductDescription(productDescription);
				setDisplayProductPrice(productPrice);
				Toast.fire({
				  icon: 'success',
				  title: `Updated Product`
				});
			} else {
				Toast.fire({
				  icon: 'error',
				  title: `Something went wrong`
				});
			}

		});

		setShowUpdateForm(false);
	}


	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{displayProductName}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{displayProductDescription}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {displayProductPrice}</Card.Text>
	        <Card.Subtitle>Availability:</Card.Subtitle>
	        { productIsActive ?
	        	<>
	        	<Card.Text><Badge bg="success">Active</Badge></Card.Text>
	        	{ showUpdateForm ?
	        		<Button className="bg-secondary" onClick={showForm}>Cancel Update</Button>
	        		:
	        		<>
	        		<Button className="bg-primary" onClick={showForm}>Update Product Information</Button>
	        		<Button className="bg-danger mx-2" onClick={e =>archiveProduct(e, _id, false)}>Deactivate Product</Button>
	        		</>
	        	}
	        	</>
	        	:
	        	<>
	        	<Card.Text><Badge bg="danger">Inactive</Badge></Card.Text>
	        	{ showUpdateForm ?
	        		<Button className="bg-secondary" onClick={showForm}>Cancel Update</Button>
	        		:
	        		<>
	        		<Button className="bg-primary" onClick={showForm}>Update Product Information</Button>
	        		<Button className="bg-success mx-2" onClick={e =>archiveProduct(e, _id, true)}>Activate Product</Button>
	        		</>
	        	}
	        	</>
	        }

	        {/*Update Form*/}
	        {	showUpdateForm ?
	        <Form onSubmit={e => updateProduct(e, _id)} className="mt-3">
	        	<Form.Group controlId="productName" className="mb-2">
	                <Form.Label>Name</Form.Label>
	                <Form.Control 
		                type="text" 
		                placeholder="Enter product name" 
		                value={productName}
		                onChange={e => setProductName(e.target.value)}
		                required
	                />
	            </Form.Group>

	            <Form.Group controlId="productDescription" className="mb-2">
	                <Form.Label>Description</Form.Label>
	                <Form.Control 
		                type="text" 
		                placeholder="Enter product description" 
		                value={productDescription}
		                onChange={e => setProductDescription(e.target.value)}
		                required
	                />
	            </Form.Group>

	            <Form.Group controlId="productPrice" className="mb-2">
	                <Form.Label>Price</Form.Label>
	                <Form.Control 
		                type="number" 
		                placeholder="Enter product price"
		                min="0" 
		                value={productPrice}
		                onChange={e => setProductPrice(e.target.value)}
		                required
	                />
	            </Form.Group>

	            <Button variant="primary" type="submit" id="submitBtn">
	            	Submit
	            </Button>

	        </Form>
	        :
	        null
	        }
	        

	    </Card.Body>
	</Card>
	)
}