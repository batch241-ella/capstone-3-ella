import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form, Stack } from 'react-bootstrap';

import {useParams, Link, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function ProductView({setCartCount}) {
	const { user } = useContext(UserContext);

	const isCartEmpty = localStorage.getItem("isCartEmpty") === "true";

	const navigate = useNavigate();

	const {productId} = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const [quantity, setQuantity] = useState(1);
	const [showQuantity, setShowQuantity] = useState(false);
	const [disableMinus, setDisableMinus] = useState(false);

	const handleQuantityChange = (value) => {
		setQuantity(currentQty => {
			return currentQty + value;
		});
	}

	const addToCart = (e, productId) => {
		e.preventDefault();

		if (isCartEmpty) {
			// Create Order
			fetch(`${process.env.REACT_APP_API_URL}/orders/addOrder`, {
	            method: 'POST',
	            headers: {
	                'Content-Type': 'application/json',
	                Authorization: `Bearer ${localStorage.getItem('token')}`
	            },
	            body : JSON.stringify({
					products: [
						{
							productId: productId,
							quantity: quantity
						}
					]
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				// Set ID of current order
				localStorage.setItem("orderId", data.orderId);

				Swal.fire({
				    title: "Cart Updated",
				    icon: "success",
				    text: "Product has been successfully added to cart"
				})
				navigate("/products");
			})
			// Set cart to not empty
			localStorage.setItem("isCartEmpty", false);
			// increment cart count after a product is added to cart
			let cartCount = parseInt(localStorage.getItem("cartCount")) + 1;
			localStorage.setItem("cartCount", cartCount);
			setCartCount(cartCount);

		} else {
			// Add product to Order (need Order ID)
			let orderId = localStorage.getItem("orderId");
			
			fetch(`${process.env.REACT_APP_API_URL}/orders/${orderId}/addProductsToOrder`, {
	            method: 'POST',
	            headers: {
	                'Content-Type': 'application/json',
	                Authorization: `Bearer ${localStorage.getItem('token')}`
	            },
	            body : JSON.stringify({
					productsToAdd: [
						{
							productId: productId,
							quantity: quantity
						}
					]
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				Swal.fire({
				    title: "Cart Updated",
				    icon: "success",
				    text: "Product has been successfully added to cart"
				})
				// increment cart count after a product is added to cart
				let cartCount = parseInt(localStorage.getItem("cartCount")) + 1;
				localStorage.setItem("cartCount", cartCount);
				setCartCount(cartCount);
				navigate("/products");
			})
		}
	}

	useEffect(() => {
		setDisableMinus(quantity === 1);
	}, [quantity])

	// Will retrieve the details of the product from our database to be displayed in the "ProductView" page
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])


	return (

		<Container className="mt-3">
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        {
					        	(user.id !== null) ?
					        		<>
					        		{ !showQuantity ?
					        			<Button className="orderBtn" variant="warning" onClick={() => setShowQuantity(true)}>Add to Cart</Button>
					        			:
					        			<>
					        			<p>Quantity</p>
					        			<Form onSubmit={(e) => addToCart(e, productId)}>
					        			<Stack className="d-flex align-items-center justify-content-center" direction="horizontal" gap={2}>
							        		<Button onClick={e => handleQuantityChange(-1)} disabled={disableMinus}>-</Button>
							        		<Form.Group controlId="productQuantity">
								                <Form.Control 
									                type="number" 
									                placeholder=""
									                min="1" 
									                value={quantity}
									                onChange={e => setQuantity(e.target.value)}
									                readOnly={true}
									                required
								                />
								            </Form.Group>
								            <Button onClick={e => handleQuantityChange(1)}>+</Button>
								        </Stack>
								        <Stack className="mt-3" direction="vertical" gap={2}>
								        	<Button type="submit" id="submitBtn" variant="success">Checkout</Button>
								        	<Button className="bg-secondary" onClick={() => setShowQuantity(false)}>Cancel</Button>
								        </Stack>
								        </Form>
					        			</>
					        		}
					        		</>
					        		:
					        		<Button className="btn btn-warning" as={Link} to="/login">Log in to Order</Button>
					        }

					      </Card.Body>
					</Card>

					<Row className="mt-2">
						<Col className="text-center">
							<Button className="btn btn-secondary" as={Link} to="/products">Back</Button>
						</Col>
					</Row>

				</Col>
			</Row>
		</Container>

	)
}