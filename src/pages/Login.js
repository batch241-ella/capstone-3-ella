import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';

import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Login() {

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);

	function nameFromEmail(email) {
		let name = email.split("@")[0];
		return name.charAt(0).toUpperCase() + name.slice(1);
	}

	function authenticate(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (typeof data.access !== "undefined") {
				// The JWT will be used to retrieve user information across the whole frontend application and storing it in the localStorage
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);
				console.log(user);

				Swal.fire({
				    title: "Login Successful",
				    icon: "success",
				    text: `Welcome back, ${nameFromEmail(email)}`
				})
			} else {
			    Swal.fire({
			    title: "Authentication Failed",
			    icon: "error",
			    text: "Please, check your login details and try again."
				})
			}
		})

		setEmail('');
		setPassword('');
		// navigate('/');

		// Set the email of the authenticated user in local storage
		// localStorage.setItem(propertyName, value)
		// localStorage.setItem('email', email);
		// Sets the global user state to have properties obtained from local storage
		// setUser({email: localStorage.getItem('email')});
	}

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			// Global user state for validation across the whole app
			// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across whole application
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
			localStorage.setItem("isAdmin", data.isAdmin);
			// For creating an empty cart
			localStorage.setItem("isCartEmpty", true);
			// For tracking the number of items in the cart
			localStorage.setItem("cartCount", 0);
		})
	}

	useEffect(() => {
		if (email && password) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])

    return (
    	(user.id !== null) ?
    	<Navigate to="/"/>
    	:
        <Form className="mt-3" onSubmit={e => authenticate(e)}>
            <Form.Group className="my-2" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value={email}
	                onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="my-2" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value={password}
	                onChange={e => setPassword(e.target.value)}
	                required
                />
            </Form.Group>

            { isActive ?
	            <Button className="mt-2" variant="success" type="submit" id="submitBtn">
	            	Submit
	            </Button>
            :
	            <Button className="mt-2" variant="success" type="submit" id="submitBtn" disabled>
	            	Submit
	            </Button>
            }
        </Form>
    )

}