import Banner from "../components/Banner";
import Highlights from "../components/Highlights";
import Footer from "../components/Footer";

import {Container, Row, Col} from 'react-bootstrap';

export default function Home() {
	const data = {
		title: "Le Pizza",
		content: "Serving quality pizza since 2012",
		destination: "/products",
		label: "View Menu"
	}

	return (
		<>
			<Container className="homeHero d-flex align-items-center justify-content-center text-light text-center">
				{/*<h4 className="bg-secondary">Text Here</h4>*/}
				<Banner data={data}/>
			</Container>
		    <Row className="mt-4">
		        <Col className="p-5 bg-cream">
		            <h3 className="heading-lg">Discover your favorite pizza</h3>
		            <p>With fresh, locally sourced ingredients, Le Pizza is your go-to place for a quick but memorable (be it dine-in or take-out) experience</p>
		        </Col>
		    </Row>
    		<Highlights />
    		<Footer/>
		</>
	)
}