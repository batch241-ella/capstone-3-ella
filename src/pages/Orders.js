import {useState, useEffect} from 'react';

// import { Card } from "react-bootstrap";

import OrderCard from "../components/OrderCard";

import Banner from "../components/Banner";

export default function Orders() {
	// State that will be used to store the products retrieved from the database
	const [orders, setOrders] = useState([]);

	const isAdmin = localStorage.getItem('isAdmin') === "true";

	// Retrives the orders from the database upon initial render of the "Orders" component
	useEffect(() => {
		if (isAdmin) {
			fetch(`${process.env.REACT_APP_API_URL}/orders/all`, {
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				// setOrders(<h1>hewwo</h1>);
				setOrders(data.map(order => {
					return (
						<OrderCard key={order._id} order={order} count={order.userId} isAdmin={true}/>
					)
				}))
			});

		} else {
			fetch(`${process.env.REACT_APP_API_URL}/orders/getOrders`, {
				method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				// console.log(localStorage.getItem('token'));
				console.log(data);

				if (data.length === 0) {
					let bannerData = {
						title: "No orders as of yet (っ °Д °;)っ",
						content: "Create an order by adding a product to your cart",
						destination: "/products",
						label: "Add Products to Cart"
					}
					setOrders(<Banner data={bannerData} />);
				} else {
					let count = 0;
					setOrders(data.map(order => {
						count++;
						return (
							<OrderCard key={order._id} order={order} count={count} isAdmin={false} />
						)
					}))
				}
				
			})
		}
	
	}, [isAdmin])


	return (
		<>
		<div className="mt-3"></div>
		<div className="parent">
			<h3 className="text-center mb-3 sectionTitle">{ isAdmin ? "All User Orders" : "My Order History"}</h3>
		</div>
		{orders}
		<div className="mb-5"></div>
		</>
	)
}