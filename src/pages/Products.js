import {useState, useEffect} from 'react';

import { Button, Container, Row, Col } from "react-bootstrap";

import {Link} from 'react-router-dom';

import ProductCard from "../components/ProductCard";
import AdminProductView from "../components/AdminProductView";

import Banner from "../components/Banner";

export default function Products() {

	let isAdmin = localStorage.getItem("isAdmin");

	// State that will be used to store the products retrieved from the database
	const [products, setProducts] = useState([]);

	// Retrives the products from the database upon initial render of the "Products" component
	useEffect(() => {
		// Admin View
		if (isAdmin === "true") {
			fetch(`${process.env.REACT_APP_API_URL}/products/all`)
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setProducts(
					<>
					<Container className="text-center py-3">
						<Button className="bg-primary" as={Link} to={`/products/createProduct`}>Create Product</Button>
					</Container>
					{data.map(product => {
						return (
							<AdminProductView key={product._id} product={product} />
						)
					})}
					</>
				)
			});
		// User View 
		} else {
			fetch(`${process.env.REACT_APP_API_URL}/products/all-active`)
			.then(res => res.json())
			.then(data => {
				console.log(data);

				// No Products 
				if (data.length === 0) {
					let bannerData = {
						title: "No products to display",
						content: "It seems there are not products availabe for purchase as of now. Come back again later!",
						destination: "/",
						label: "Back to Home"
					}
					setProducts(<Banner data={bannerData} />);
				// With Products
				} else {
					// setProducts(data.map(product => {
					// 	return (
					// 		<ProductCard key={product._id} product={product} />
					// 	)
					// }));

					let rowList = [];
					let colList = [];
					let count = 0;
					for (let i = 0; i < data.length; i++) {
						if (count === 3) {
							// finish row
							rowList.push(
								<Row key={rowList.length}>
									{colList}
								</Row>
							);
							colList = [];
							count = 0;
						} 
						// build col list
						colList.push(
							<Col key={i}><ProductCard key={data[i]._id} product={data[i]} /></Col>
						)
						count++;
						// console.log("index is " + i)
						
					}
					// add leftovers to row list
					if (colList.length > 0) {
						rowList.push(
							<Row key={rowList.length}>
								{colList}
							</Row>
						)
					}
					setProducts(rowList);
				}
			});
		}
		
	}, [isAdmin])


	return (
		<>

		<Container className="mt-3 mb-3">
			<Row>
				<Col className="text-center"><h3 className="sectionTitle">Our Products</h3></Col>
			</Row>
			{products}
		</Container>
		</>
	)
}