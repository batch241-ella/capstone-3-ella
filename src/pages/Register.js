import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';

import {useNavigate, Navigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Register() {

	const navigate = useNavigate();

	const {user, setUser} = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether submit button will be enabled or not
	const [isActive, setIsActive] = useState(false);


	// Function to simulate user registration
	function registerUser(e) {
		e.preventDefault();

		if(mobileNo.length < 11) {
			Swal.fire({
			    title: "Incorrect Mobile Number",
			    icon: "error",
			    text: "Mobile number must be at least 11 digits, please try again"
			})
			return;
		}

		if(password1 !== password2) {
			Swal.fire({
			    title: "Password mismatch",
			    icon: "error",
			    text: "Passwords do not match, please try again"
			})
			return;
		}

		// Register user via POST request to server
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			// Not duplicate
			if (!data) {
				console.log(data);
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					firstName: firstName,
					lastName: lastName,
					email: email,
					mobileNo: mobileNo,
					password: password1
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				if (data) {
					Swal.fire({
					    title: "Registration Complete",
					    icon: "success",
					    text: "Congratulations, you have been successfully registered!"
					})
					navigate("/login");
				} else {
					Swal.fire({
					    title: "Registration Failed",
					    icon: "error",
					    text: "Something went wrong, please try again"
					})
				}
			})
			// duplicate email found
		} else {
			Swal.fire({
			    title: "Email Already Taken",
			    icon: "error",
			    text: "This email is already in use, please choose another email"
			})
			return;
		}

		});
		
		
		// clear input fields
		setFirstName('');
		setLastName('');
		setEmail('');
		setMobileNo('');
		setPassword1('');
		setPassword2('');

	}


	useEffect(() => {
		// Validation to enable submit button when all fields are populated and both passwords match
		if (firstName && lastName && email && mobileNo && mobileNo.length >= 11 && password1 && password2 && password1 === password2) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobileNo, password1, password2])

    return (
    	(user.id !== null) ?
    	<Navigate to="/"/>
    	:
        <Form className="mt-3" onSubmit={e => registerUser(e)}>
        	<Form.Group className="my-2" controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter first name" 
	                value={firstName}
	                onChange={e => setFirstName(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group className="my-2" controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter last name" 
	                value={lastName}
	                onChange={e => setLastName(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group className="my-2" controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                value={email}
	                onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>


            <Form.Group className="my-2" controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter Mobile Number" 
	                value={mobileNo}
	                onChange={e => setMobileNo(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group className="my-2" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                value={password1}
	                onChange={e => setPassword1(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group className="my-2" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password" 
	                value={password2}
	                onChange={e => setPassword2(e.target.value)}
	                required
                />
            </Form.Group>
            {/* conditionally render submit button based on "isActive" state */}
            { isActive ?
            <Button className="mt-2" variant="primary" type="submit" id="submitBtn">
            	Submit
            </Button>
            :
            <Button className="mt-2" variant="danger" type="submit" id="submitBtn">
            	Submit
            </Button>
            }
        </Form>
    )

}