import { Table, Row, Col, Badge, Button } from 'react-bootstrap';

import {useState, useEffect, Fragment} from 'react';

import Swal from 'sweetalert2';

export default function Users() {

    const [users, setUsers] = useState([]);

    const [isUserAdmin, setIsUserAdmin] = useState(false);

    function showSetAdminModal(e, userId, userFirstName, isAdmin) {
        e.preventDefault();

        if (!userFirstName) {
            userFirstName = "User";
        }

        if (isAdmin) {
            Swal.fire(
              'Redundant Action',
              `${userFirstName} is already an Admin!`,
              'warning'
            )
            return;
        }

        Swal.fire({
          title: `Set ${userFirstName} as Admin?`,
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes'
        }).then((result) => {
          if (result.isConfirmed) {
            fetch(`${process.env.REACT_APP_API_URL}/users/${userId}/setAdmin`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => res.json())
            .then(wasUpdated => {
                console.log(wasUpdated);

                if (wasUpdated) {
                    Swal.fire(
                      'Administrative Priveleges Granted',
                      `${userFirstName} has been promoted to Administrator`,
                      'success'
                    )
                    setIsUserAdmin(true);
                } else {
                    Swal.fire(
                      'An unexpected error occured',
                      '(╯‵□′)╯︵┻━┻',
                      'error'
                    )
                }
            })
           
          }
        })
    }

	
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/users/all`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setUsers(data.map(user => {
                return (
                    <Fragment key={user._id}>
                    <tr>
                        <th className="text-info font-weight-normal">
                            <Button variant="bg-transparent text-info" onClick={e =>showSetAdminModal(e, user._id, user.firstName, user.isAdmin)}>{user._id}</Button>
                        </th>
                        <th>{user.isAdmin ? <Badge bg="warning">Administrator</Badge> : <Badge bg="success">User</Badge>}</th>
                        <th className="font-weight-normal">{user.firstName}</th>
                        <th className="font-weight-normal">{user.lastName}</th>
                        <th className="font-weight-normal">{user.email}</th>
                        <th className="font-weight-normal">{user.mobileNo}</th>
                    </tr>
                    </Fragment>
                )})
            )
            console.log(users)
        });
    }, [isUserAdmin, users])


	return (
    <>
    <Row>
        <Col className="p-3">
            <h2>User Information</h2>
            <p>Click on a user's ID to set them to Administrator</p>
        </Col>
    </Row>

    <Table striped bordered hover responsive="sm" className="text-center">
      <thead>
        <tr>
          <th>User ID</th>
          <th>Role</th>
          <th>First Name</th>
          <th>Last Name</th>
          <th>Email</th>
          <th>Mobile Number</th>
        </tr>
      </thead>
      <tbody>
        {users}
      </tbody>
    </Table>
    </>
    )
}